import time

import cv2
import sys

# root_dir = "/Users/artemredchych/FIT/ING/BML/kalman-bear"
# sys.path.append(root_dir)

import libs.tracking.kalman as kalman
import libs.detection.final_detection as det

def init_kalman():
    return kalman.initialize_kalman_filter()

def process_imageOof(model, image, kf, is_first, frames_since_last_detection):
    # Always predict the state first
    start_time = time.time()
    predicted_state = kf.predict()
    # kf.statePost = predicted_state
    predicted_box = kalman.state_to_bbox(predicted_state)
    end_time = time.time()
    print(f"Kalman prediction executed in: {end_time - start_time} seconds")

    # Run inference on the image
    output_dict = det.run_inference_for_single_image(model, image)
    # Draw bounding boxes and class names on the image
    detection = det.get_bounding_box(output_dict=output_dict, image=image, threshold=0.9)

    if detection is not None:
        bbox, class_name, score = detection
        result_image = det.draw_detection_bbox(image, bbox, score, class_name)
        frames_since_last_detection = 0  # Reset counter since we have a detection

        if is_first:
            initial_bbox = bbox
            # Set the initial state
            kf.statePost = kalman.bbox_to_state(initial_bbox)
        else:
            # Update the state with the new measurement
            kf.correct(kalman.bbox_to_state(bbox))
            result_image = det.draw_predicted_bbox(result_image, predicted_box, (0, 0, 255))
    else:
        frames_since_last_detection += 1
        result_image = image.copy()
        if frames_since_last_detection <= 10:
            # Draw the predicted box even if there's no detection, for up to 10 frames
            result_image = det.draw_predicted_bbox(result_image, predicted_box, (0, 255, 0))
        else:
            # Reset Kalman filter or handle lost track appropriately
            print("Object lost for more than 10 frames.")

    return result_image, frames_since_last_detection, is_first
def process_image(model, image, kf, is_first, frames_since_last_detection):
    # Run inference on the image
    output_dict = det.run_inference_for_single_image(model, image)
    # Draw bounding boxes and class names on the image
    detection = det.get_bounding_box(output_dict=output_dict, image=image, threshold=0.9)
    if detection is not None:
        bbox, class_name, score = detection
        result_image = det.draw_detection_bbox(image, bbox, score, class_name)
        frames_since_last_detection = 0  # Reset counter since we have a detection

        if is_first:
            initial_bbox = bbox
            # Set the initial state
            kf.statePost = kalman.bbox_to_state(initial_bbox)
        else:
            start_time = time.time()

            predicted_state = kf.predict()
            predicted_box = kalman.state_to_bbox(predicted_state)

            # Update the state with the new measurement
            kf.correct(kalman.bbox_to_state_oof(bbox))
            end_time = time.time()
            execution_time = end_time - start_time
            print(f"Kalman executed in: {execution_time} seconds")
            result_image = det.draw_predicted_bbox(result_image, predicted_box, (255, 0, 0))

        return result_image, frames_since_last_detection, is_first
    else:
        frames_since_last_detection += 1
        predicted_state = kf.predict()
        predicted_box = kalman.state_to_bbox(predicted_state)
        result_image = image.copy()
        if frames_since_last_detection <= 10:
            # Draw the predicted box even if there's no detection, for up to 10 frames
            result_image = det.draw_predicted_bbox(result_image, predicted_box, (255, 0, 0))
        else:
            # Reset Kalman filter or handle lost track appropriately
            print("Object lost for more than 10 frames.")

    return result_image, frames_since_last_detection, is_first


