import os
import cv2


import time
import libs.detection.final_detection as det
import libs.image_processing as ip
import libs.tracking.kalman as kalman

def process_frame(image_path, model, kf, is_first, frames_since_last_detection):
    color_img = cv2.imread(image_path)
    if color_img is not None:
        result_image, frames_since_last_detection, is_first = ip.process_image(
            model=model,
            image=color_img,
            kf=kf,
            is_first=is_first,
            frames_since_last_detection=frames_since_last_detection
        )
        return result_image, frames_since_last_detection, is_first
    return None, None, None

def main(folder_path):
    model = det.load_model('./inference_graph')
    kf = kalman.initialize_kalman_filter()
    is_first = True
    frames_since_last_detection = 0

    image_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg') or f.endswith('.png')]
    image_files.sort()  # Ensure files are processed in order

    # Generate a timestamp and create the output directory
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    output_dir = os.path.join('./output', timestamp)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for idx, image_file in enumerate(image_files):
        image_path = os.path.join(folder_path, image_file)
        result_image, frames_since_last_detection, is_first = process_frame(image_path, model, kf, is_first, frames_since_last_detection)

        if result_image is not None:
            is_first = False
            cv2.imshow('result', result_image)
            save_path = os.path.join(output_dir, f"processed_{idx}.jpg")
            cv2.imwrite(save_path, result_image)
        else:
            cv2.imshow('result', cv2.imread(image_path))
            save_path = os.path.join(output_dir, f"original_{idx}.jpg")
            cv2.imwrite(save_path, image_path)

        # Wait for the right arrow key to be pressed to move to the next frame
        while True:
            key = cv2.waitKey(0)
            print(f"Key pressed: {key}")
            if key == 110:  # Right arrow key
                break

if __name__ == "__main__":
    main('./data/lab_test_final_raw4')
