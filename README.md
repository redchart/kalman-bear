# Kalman-Bear Project

## Overview
This project is part of the NI-BML (Bayesian Methods in Machine Learning) course, featuring the implementation of a Kalman filter for tracking a teddy bear in video frames. A trained MobilenetSSD model detects the teddy bear, highlighted with a green bounding box. The Kalman filter's prediction is marked by a blue bounding box, continuing predictions up to 10 frames after the bear disappears, helping estimate its position in cases of occlusion.

## Installation
All project dependencies are listed in the `requirements.txt` file. Install them using the following command:

`pip install -r requirements.txt`



## Usage
- **Input Data**: Place input files in the `data` folder, contained within `lab_data.zip`.
- **Running the Project**: To start tracking, run the script with `python bear-kalman.py`. The user will need to press `N` to continue to each subsequent frame.
- **Output**: The output is shown in a live window and also stored in the `output` directory.


## Contributing
Guidelines for contributing to the project, including coding standards and pull request submission.

## License
Licensing information for the project.

## Contact
Contact information for project-related queries.
